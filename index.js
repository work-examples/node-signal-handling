let loop = true;

async function main() {
    let i = 0;
    while(loop) {
        i++;
        console.log(`[Main Program Loop] Loop ${i}...`);
        await new Promise(r => setTimeout(r, 10000));
    }

    console.log('[Main Program Loop] Finished!');
}

async function gracefulShutdown() {
    console.log('[Shutdown Loop] Preparing to shut down gracefully');
    let i = 5;
    for(i = 5;i>0;--i) {
        console.log(`[Shutdown Loop] ${i}...`);
        await new Promise(r => setTimeout(r, 1000));
    }
    loop = false;
}

process.on('SIGTERM', () => {
    console.log('[Signal Handling] Received SIGTERM. Killing loop...');
    gracefulShutdown();
});

process.on('SIGINT', () => {
    console.log('[Signal Handling] Received SIGINT. Killing loop...');
    gracefulShutdown();
});

main();