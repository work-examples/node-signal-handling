FROM node:10-slim
WORKDIR /usr/local/app
COPY . .

CMD [ "node","index.js" ]